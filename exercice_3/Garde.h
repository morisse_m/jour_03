#ifndef GARDE_H_
# define GARDE_H_

#include "bc.h"
#include <stdlib.h>

typedef enum {STARK, LANNISTER, TARGARYEN, BARATHEON, MARTELL, TYRELL} Maison;

typedef struct corbeau
{
  char *_nom;
  int _age;
  Maison _maison;
} Corbeau;

void garde_de_nuit(Corbeau*, char*, int, Maison);

#endif /* !GARDE_H_ */
